## Demo Project:
Automation with Python
## Technologies used:
Python, IntelliJ, Git
## Project Decription:
* Write an application that reads a spreadsheet file and process and manipulate the spreadsheet
## Description in details:
You have 4 taks:
1. List each company with respective product count
2. List each company with respective total inventory value
3. List products with inventory less than 10  
4. Write to Spreadsheet: Calculate and write inventory value for each product into spreadsheet

__Task 1:__ List each company with respective product count
1. Install `openpyxl` - package for working with spreadsheets
2. Import module
3. Use function to read file (and assign file)
```py
inv_file = openpyxl.load.workbook("inentory.xlsx")
```
This file 4 colums and you to create variables for them
```py
product_list = inv_file["Sheet1"]
```
1. Create dictionary 
```py
products_per_supplier = {}
```
1. Write loop for count 
```py
for product_rof in range(2, product_list.max_row + 1):
    supplier_name = product_list.cell(product_row, 4).value

    if supplier_name in product_per_supplier:
    currnet_num_of_products = products_per_supplier.get(supplier_name)
    products_per_supplier[supplier_name]= products_per_supplier +1 
    else:
    print ("adding new supplier")
    products_per_supplier[supplier_name] = 1

print(products_per_supplier) 
```
>- `"range"` - use range because the `for` loop is for iterating over a list 
>- `"+ 1"`  - used for include the last number 
>- `"2"` - you start from second line because first line for column names. You don't start here from `0` because you dont have this line obviously


__Task 2:__ List each company with respective total inventory value
1. Create new dictionary 
```py
total_value_pet_supplier{}
```
1. Add `inventory` and `price` variables 
```py
inventory = product_list.cell(product_row, 2).value
price = product_list.cell(product_row, 3).value
```
1. Create calculation of total value of inventory per supplier
```py
if supplier_name in total_value_pet_supplier()
    current_total_value = total_value_pet_supplier.get(supplier_name)
    total_value_pet_supplier(supplier_name) = current_total_value + inventory * price
else
    total_value_pet_supplier[supplier_name] = inventory * price
```

__Task 3:__ List products with inventory less than 10
 
1. Create new variable for task
```py 
products_under_10_inv = {}
```
2. create variable for first row to know what is product number
```py 
product_num = product_list.cell(product_row, 1).value
```
3. write logic
```py
if inventory < 10:
products_under_10_inv[product_num] = int.inventory
```
__Task 4:__  Write to Spreadsheet: Calculate and write inventory value for each product into spreadsheet

1.  Create 5th row
```py
inventory_price = product_list.cell(product_row, 5)
```
2. Write logic
```py
inventory_price.value = inventory * price
```
3. Create new file with new result in 5th row
```py
inv_file.save("invetory_with_total_value.xlsx")
```


## Completed code:
```py
import openpyxl

inv_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inv_file["Sheet1"]

products_per_supplier = {}
total_value_per_supplier = {}
products_under_10_inv = {}

for product_row in range(2, product_list.max_row + 1):
    supplier_name = product_list.cell(product_row, 4).value
    inventory = product_list.cell(product_row, 2).value
    price = product_list.cell(product_row, 3).value
    product_num = product_list.cell(product_row, 1).value
    inventory_price = product_list.cell(product_row, 5)

    # calculation number of products per supplier
    if supplier_name in products_per_supplier:
        current_num_products = products_per_supplier.get(supplier_name)
        products_per_supplier[supplier_name] = current_num_products + 1
    else:
        products_per_supplier[supplier_name] = 1

    # calculation total value of inventory per supplier
    if supplier_name in total_value_per_supplier:
        current_total_value = total_value_per_supplier.get(supplier_name)
        total_value_per_supplier[supplier_name] = current_total_value + inventory * price
    else:
        total_value_per_supplier[supplier_name] = inventory * price

    # logic products with inventory less than 10
    if inventory < 10:
        products_under_10_inv[int(product_num)] = int(inventory)

    # add value for total inventory price
    inventory_price.value = inventory * price


print(products_per_supplier)
print(total_value_per_supplier)
print(products_under_10_inv)

inv_file.save("inventory_with_total_value.xlsx")
```
