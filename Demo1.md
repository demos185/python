## Demo Project:
Write Countdown Application
## Technologies used:
Python, IntelliJ, Git
## Project Decription:
* Write a napplication that accepts a user input of a goal and a deadline (date). Print the remaining time until that deadline
## Description in details:
__Step 1.__ Import datetime module
```py
import datetime
```

__Step 2:__ Create user input
```py
user_input("enter your goal with a deadline separated by colon(deadline name:day.moth.full year)\n")
user_input.split(":")
```
- `user_input.split(":")` - use this because it will be list

__Step 3:__ Extract individual values using this syntax
```py
goal = user_input[0]
deadline = user_input[1]
```

__Step 4:__ Use module (Read docs before using any module!)

```py
deadline_date = datetime.datetime.strprime(deadline, "%d.%m.%Y") 
```

__Step 5:__ Create variable for calculation

```py
today_date = datetime.datetime.today()
```

__Step 6:__ Calculation

```py 
print(deadline_date - today_date)  
```

__Step 7:__ Create message for user

1. Create variable for result of calculation
```py
time_till = deadline_date - today_date
```
2. Write message for user
```py
print (f"Dear user! Time remaining for your goal: {goal} is {time_till}")
```
__Step 8:__ Correcting output

Convert result into days
```py
print (f"Dear user! Time remaining for your goal: {goal} is {time_till.days} days")
```
__Step 9:__ Convert days to hours

```py
print (f"Dear user! Time remaining for your goal: {goal} is {int(time_till.total_seconds() / 60 / 60)} hours")
```
__Note:__ create variable to make code cleaner

```py
hours_till = int(time_till.total_seconds() / 60 / 60)
```
```py
print (f"Dear user! Time remaining for your goal: {goal} is {hours_till} hours") 
```

## Complete code:
```py
calculation_to_units = 24
name_of_unit = "hours"


def days_to_units(num_of_days):
    return f"{num_of_days} days are {num_of_days * calculation_to_units} {name_of_unit}"


def validate_and_execute():
    try:
        user_input_number = int(num_of_days_element)

        # we want to do conversion only for positive integers
        if user_input_number > 0:
            calculated_value = days_to_units(user_input_number)
            print(calculated_value)
        elif user_input_number == 0:
            print("you entered a 0, please enter a valid positive number")
        else:
            print("you entered a negative number, no conversion for you!")
    except ValueError:
        print("your input is not a valid number. Don't ruin my program!")


user_input = ""
while user_input != "exit":
    user_input = input("Hey user, enter number of days as a comma separated list and I will convert it to hours!\n")
    list_of_days = user_input.split(", ")
    for num_of_days_element in set(list_of_days):
        validate_and_execute()

```