## Demo Project:
API Request to GitLab
## Technologies used:
Python, GitLab, IntelliJ, Git
## Project Decription:
* Write an application that talks to an API of an external application (GitLab) and lists all the public GitLab repositories for a specified user
## Description in details:
__Step 1:__ Install module `requests` 

__Step 2:__ Write an application
```py
import requests

response = requests.get("https://gitla.com/api/v4/users/:user_id/projects")
my_projects = response.json()

for project in my_projects:
    print(f"Project Name: {project['name']}\n Project Url: {project['web_url']}\n")

```
- `requests.get` Here you need eddit user ID. 
- `response.json` Use json because:
  * Lightweght format for __transporting data__
  * Often used to send data over the web
- `project['name']` use single quotes because a string used inside another string 

__Note 1:__ You can see names of elements using `print command`

Example:
```py
import requests

response = requests.get("https://gitla.com/api/v4/users/:user_id/projects")
print(response.json)

```
